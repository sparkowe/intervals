<?php

namespace Sparkowe\Intervals\App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Sparkowe\Intervals\Classes\Intervals;


class IntervalsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * index
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(view('Sparkowe\Intervals::index'));
    }



    /**
     * Return specified parameter's intervals json data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $filepath = request('filepath', null);
        $parameter = request('parameter', null);
        $descending = filter_var(request('descending', null), FILTER_VALIDATE_BOOLEAN);

        $data = [];

        if ($filepath) {
            $data = Intervals::getParamIntervals(storage_path('app/'.$filepath), $parameter, $descending);
        }

        return response()->json($data);
    }



    /**
     * Upload json and return json parameters list
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $data = array();
        $validator = Validator::make($request->all(), ['file' => 'required|file|mimetypes:application/json,text/plain']);

        if ($validator->fails()) {
            $data['success'] = 0;
            $data['error'] = $validator->errors()->first('file');

        } else {
            if ($request->file('file')) {
                $file = $request->file('file');
                $filename = time().'_'.$file->getClientOriginalName();
                $filepath = Storage::putFileAs('', $file, $filename);

                $data['success'] = 1;
                $data['message'] = 'Uploaded Successfully!';
                $data['filepath'] = $filepath;

                $data['parameters'] = Intervals::getDataParameters(storage_path('app/'.$filepath));

            } else {
                $data['success'] = 2;
                $data['message'] = 'File not uploaded.';
            }
        }

        return response()->json($data);
   }

}