<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>

		<!-- Bootstrap -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/js/bootstrap.bundle.min.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css" rel="stylesheet" >

		<!-- DataTables -->
		<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
		<link href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css" rel="stylesheet">

		<style>
			.sidebar {
				position: fixed;
				height: 100%;
				left: 0;
				z-index: 100;
			}

			@media (max-width: 767.98px) {
				.sidebar {
					top: 5rem;
				}
			}

			.sidebar-sticky {
				height: calc(100vh - 85px);
				overflow-x: hidden;
				overflow-y: auto;
			}
		</style>

	</head>
	<body>

		<header class="navbar bg-light sticky-top flex-md-nowrap py-3 px-3 border shadow-sm">

			<div class="col-md-2">
				<h5 class="py-0 my-0">Параметры</h5>
			</div>

			<div class="col-md-2">
				<div class="form-check form-switch">
					<input class="form-check-input" type="checkbox" role="switch" id="checkDesc">
					<label class="form-check-label" for="checkDesc">Убывание</label>
				</div>
			</div>

			<div class="col-md-2 p-2">
				<div class="input-group">
					<span class="input-group-text">От</span>
					<input type="number" step="1" id="min" class="form-control">
				</div>
			</div>
			<div class="col-md-2 p-2">
				<div class="input-group">
					<span class="input-group-text">До</span>
					<input type="number" step="1" id="max" class="form-control">
				</div>
			</div>

			<div class="col-md-4 d-flex align-items-center flex-md-nowrap justify-content-end border rounded bg-white p-1">

				<div id="file_title" class="rounded px-2 py-2 mx-2 text-nowrap text-truncate">
					<span class="text-muted">Выберите файл с данными</span>
				</div>

				<form class="text-nowrap m-0">
					@csrf
					<input type="file" id="file" name="file" class="d-none">
					<label for="file" class="btn btn-primary btn-large">
						<i class="bi bi-upload"></i>
						<span class="px-1"></span>Загрузить...
					</label>
				</form>

			</div>

		</header>


		<div class="container-fluid">
			<div class="row">

				<nav id="sidebarMenu" class="col-md-2 d-md-block bg-light sidebar collapse px-0">
					<div class="position-sticky sidebar-sticky">
						<div id="parameters" class="list-group list-group-flush pt-2"></div>
					</div>
				</nav>

				<main class="col-md-10 ms-sm-auto py-3">
					<div class="table-responsive">
						<table id="dataTable" class="table table-striped">
							<thead>
								<tr>
									<th>Начало интервала</th>
									<th>Конец интервала</th>
									<th>Начальное значение</th>
									<th>Конечное значение</th>
									<th>Δ</th>
								</tr>
							</thead>
						</table>
					</div>
				</main>

			</div>
		</div>

		<script type="text/javascript">

			var	parameters = null,
				filename = null,
				filepath = null,
				parameter = null,
				descending = false

			var minEl = $('#min')
			var maxEl = $('#max')

			var table = null

			$(document).ready(function() {

				$.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
					let min = parseFloat(minEl.val());
					let max = parseFloat(maxEl.val());
					return (
						(isNaN(min) && isNaN(max)) ||
						(data[2] >= min && data[3] <= max)
					)
				})

				$("#checkDesc").change(function() {
					descending = this.checked
					showData()
				})

				//$('#submitUpload').click(function() {
				$("#file").change(function () {

					let files = $('#file')[0].files;
					if (files.length > 0) {
						let file = files[0];

						let formData = new FormData();
						formData.append('file', file);

						$.ajax({
							url: "{{ route('upload') }}",
							method: 'post',
							data: formData,
							dataType: 'json',
							processData: false,
							contentType: false,
							success: function(response) {
								if(response.success == 1) {
									filename = file.name
									filepath = response.filepath
									showData()
								} else if (response.success == 2) {
									console.log("Server upload error:", JSON.stringify(response));
								} else {
									console.log("Upload error:", JSON.stringify(response));
								}
							},
							error: function(response){
								console.log("Upload ajax error:", JSON.stringify(response));
							}
						})
					}
				})

				//uploadFile("http://localhost/telemetry_20230406.json");

			})



			function showData() {
				if (!filepath) {
					return
				}

				$("#file_title").html('<span class="text-muted">Выберите файл с данными</span>');

				table = $('#dataTable')
					.on( 'error.dt', function (e, settings, techNote, message) {
						console.log('DataTables error:', message);
					})
					.DataTable({
						// stateSave: true,
						bDestroy: true,
						autoWidth: false,
						lengthMenu: [
                            [15, 30, 60, -1],
                            [15, 30, 60, 'All'],
                        ],
						ajax: {
							url: '/intervals/data',
							dataType: 'json',
							data: {
								filepath: filepath,
								parameter: parameter,
								descending: descending,
							},
							complete: function (json, type) {
								if (type == "success") {
									$("#file_title").text(filename);

									descending = json.responseJSON.descending
									parameters = json.responseJSON.parameters
									parameter = json.responseJSON.parameter

									const min_value = json.responseJSON.min_value
									const max_value = json.responseJSON.max_value

									$("#min").attr({
										"min" : min_value,
										"max" : max_value,
									}).val(min_value)

									$("#max").attr({
										"min" : min_value,
										"max" : max_value,
									}).val(max_value)

									table.draw();

									$("#checkDesc")[0].checked = descending

									console.log("min, max:", min_value, max_value)
									showParameters(parameters, parameter)
								} else {
									console.log("Server data error", json.responseText)
								}
							}
						},
						columns: [
							{ data: 'start' },
							{ data: 'end' },
							{ data: 'start_value' },
							{ data: 'end_value' },
							{ data: 'delta' },
						],
					});

				minEl.on('input', function () {
					table.draw()
				})

				maxEl.on('input', function () {
					table.draw()
				})

			}


			function showParameters(parameters, activeParameter) {
				const parametersList = $("#parameters").empty()
				$.each(parameters, function(index, parameter) {
					$("<a></a>")
						.addClass("item-parameter list-group-item list-group-item-action")
						.attr("href", "#")
						.attr("data-name", parameter)
						.text(parameter)
						.toggleClass("active", parameter == activeParameter)
						.attr("aria-current", parameter == activeParameter)
						.click(selectParameter)
						.appendTo(parametersList)
				})
			}

			function selectParameter() {
				parameter = $(this).data("name")
				showData()
			}


			// Test - AutoUpload
			function uploadFile(url) {
				fetch(url)
					.then(response => response.text())
					.then(text => {
						const myFile = new File([text], url, { type: 'application/json', lastModified: new Date() })
						const dataTransfer = new DataTransfer()
						dataTransfer.items.add(myFile)
						$("#file")[0].files = dataTransfer.files
						$("#file").change()
					})
			}

		</script>

	</body>
</html>