<?php

namespace Sparkowe\Intervals;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;

class IntervalsProvider extends ServiceProvider
{
    /**
     * Bootstrap
     *
     * @return void
     */
    public function boot()
    {
        // Include the package classmap autoloader
        if (File::exists(__DIR__.'/../vendor/autoload.php'))
        {
            include __DIR__.'/../vendor/autoload.php';
        }

        /**
        * Routes
        */

        $this->app->router->group(['namespace' => 'Sparkowe\Intervals\App\Http\Controllers'],
            function(){
                require __DIR__.'/routes/web.php';
            }
        );

        /**
        * Views
        */

        $this->loadViewsFrom(__DIR__.'/resources/views', 'Sparkowe\Intervals');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Intervals', function(){
            return $this->app->make('Sparkowe\Intervals\Classes\Intervals');
        });
    }

}