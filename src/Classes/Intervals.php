<?php

namespace Sparkowe\Intervals\Classes;

class Intervals {

    /**
     * Return data section from json file
     *
     * @return array
     */
    private static function getJsonData($filepath) {
        $json = json_decode(file_get_contents($filepath), true);
        if (!$json || !isset($json['data'])) {
            return [];
        }

        $data = $json['data'];
        return count($data) ? $data : [];
    }

    /**
     * Return interval data array
     *
     * @return array
     */
    private static function intervalDataArray($start, $end, $start_value, $end_value) {
        $delta = round($end_value-$start_value, 2);
        return [
            "start" => date("Y-m-d H:i:s", $start),
            "end" => date("Y-m-d H:i:s", $end),
            "start_value" => $start_value,
            "end_value" => $end_value,
            "delta" => $delta
        ];
    }

    /**
     * Return Intervals data array
     *
     * @return array
     */
    public static function getParamIntervals(string $filepath, string|null $parameter, bool $descending = False) {
        $intervals = [];
        $start = null;
        $prev_value = null;
        $prev_datetime = null;
        $start_value = null;
        $min_value = 0;
        $max_value = 0;

        $data = self::getJsonData($filepath);
        $parameters = self::getDataParameters($data);

        // check parameters
        if (is_array($parameters)) {
            // if invalid parameter is specified, use first available
            if (!in_array($parameter, $parameters)) {
                $parameter = $parameters[0];
            }
        } else {
            return [];
        }

        foreach ($data as $record) {
            $datetime = strtotime($record['datetime']);

            $value = round($record[$parameter], 3);

            if ($value <= $min_value) {
                $min_value = $value;
            } else {
                $max_value = $value;
            }

            $condition = $descending ? $value <= $prev_value : $value >= $prev_value;

            if ($prev_value === null || $condition) {
                // open interval
                if ($start === null) {
                    $start = $prev_datetime;
                    $start_value = $prev_value;
                }
            } else {
                // close interval
                if ($start !== null) {
                    $end = $prev_datetime;
                    $end_value = $prev_value;
                    if ($start_value != $end_value) {
                        $intervals[] = self::intervalDataArray($start, $end, $start_value, $end_value);
                    }
                    $start = null;
                }
            }

            $prev_datetime = $datetime;
            $prev_value = $value;
        }

        // close last interval
        if ($start !== null) {
            $end = $prev_datetime;
            $end_value = $prev_value;
            if ($start_value != $end_value) {
                $intervals[] = self::intervalDataArray($start, $end, $start_value, $end_value);
            }
        }

        $result = [
            "data" => $intervals,
            "parameters" => $parameters,
            "parameter" => $parameter,
            "descending" => $descending,
            "min_value" => $min_value,
            "max_value" => $max_value
        ];

        return $result;
    }

    /**
     * Return Parameters array
     *
     * @return array
     */
    public static function getDataParameters(string|array $jsonData)
    {
        $data = is_array($jsonData) ? $jsonData : self::getJsonData($jsonData);
        if (!count($data)) {
            return [];
        }

        //$keys = array_values(array_diff(array_keys($data[0]), ['datetime']));
        $keys = array_keys(array_filter($data[0], function($v, $k) { return is_numeric($v); }, ARRAY_FILTER_USE_BOTH));

        return $keys;
    }


}