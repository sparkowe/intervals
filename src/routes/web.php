<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/intervals', 'IntervalsController@index');
Route::get('/intervals/data', 'IntervalsController@data');
Route::post('/intervals/upload', 'IntervalsController@upload')->name('upload');
